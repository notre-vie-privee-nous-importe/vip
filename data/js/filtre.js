self.port.on('nettoyer', function () {

    //Attention, des blocs de commentaires peuvent contenir des liens ou du code :
    //Ex : <!--[if lte IE 7]>
    //         <link href="//mozorg.cdn.mozilla.net/media/css/oldIE-bundle.ea7fe0ba08ae.css" rel="stylesheet" type="text/css" />
    //     <![endif]-->
    //Nous les supprimons donc !
    var filter = function (index, element) {
        return element.nodeType == 8;
    };

    var inhiber = function (index, element) {
        element.nodeValue = "#";
    };

    //Facebook et google n'envoient pas de données à un concurrent, pas la peine de bloquer leurs services...
    //Pour les autres, pas de pitié, pourquoi un site enverrait-il, dans le dos de son visiteur, des données de navigation ?
    if (!document.location.host.startsWith('www.facebook.') && !document.location.host.startsWith('www.google.')) {
        $("*").contents().filter(filter).each(inhiber);
        $("document").contents().filter(filter).each(inhiber);
        $(":root").contents().filter(filter).each(inhiber);
    }


    //Tous les scripts  inline sont supprimés
    $("script").each(function (index, element) {
        var src = $(this).attr('src');
        //Désactivation des scripts qui n'ont pas de sources extérieures et localisées de manière identifiable sur un serveur.
        if (!src) {
            $(this).replaceWith("<script></script>");
        }
    });


    // Pour que les utilisateurs de google et de Qwant puissent les utiliser avec ce plugin tout en conservant un minimum de vie privée.
    // Il est en effet très difficile de faire autrement car une fois envoyé sur un site google ou qwant avant d'arriver sur le vrai lien,
    // le plugin a déjà noté comme seule url autorisée: google ou qwant ... Ce qui empêche d'atteindre le lien final car son hôte sera bloqué.
    // de plus, ceci qui nuit fortement à la vie privée à la vie privée de tous les utilisateurs...
    // Non seulement google/qwant/autres connaissent la recherche de l'utilisateur mais en plus, il pourraient savoir quel lien a été choisit
    // et renvoyer toutes ces informations au site final sans que l'utilisateur n'en ait conscience...
    if (document.location.host.startsWith('www.google.')) {
        var googleTranslate = function (index, element) {
            var params = $(element).attr('href').split('&');
            $(element).attr('onclick','');
            $(element).attr('onmousedown','');
            if (params[0]) {
                var urlGoogle = params[0].split('q=');
                if (urlGoogle) {
                    var urlRéèlle = urlGoogle[urlGoogle.length - 1].split(':');
                    if (urlRéèlle) {
                        $(element).attr('href', decodeURIComponent(urlRéèlle[urlRéèlle.length - 1]));
                    } else {
                        $(element).attr('href', decodeURIComponent(urlGoogle[urlGoogle.length - 1]));
                    }
                    $(element).attr('target', '_blank');
                }
            }
        }
        $("a[href^='/url']").each(googleTranslate);
    }
    else if (document.location.host.startsWith('lite.qwant.com')) {
        var qwantTranslate = function (index, element) {
            var params = $(element).attr('href').split('%3D/');
            if (params[1]) {
                var urlRéèlle = params[1].split('?') [0];
                $(element).attr('href', decodeURIComponent(urlRéèlle));
                $(element).attr('target', '_blank');
            }
        }
        $("a[href^='/redirect']").each(qwantTranslate);
    }
    else {
        $("a[href^='http']").each(function (i, e) {
            var href = $(e).attr('href');
            if (href.startsWith(document.location.protocol + '//' + document.location.host) === false) {
                $(e).attr('target', '_blank');
            }
        });
    }
});

self.port.on('localStorage', function () {
    if (localStorage) {
        localStorage.clear();
    }
    if (window.localStorage) {
        window.localStorage.clear();
    }
});


