#!/bin/bash

CHEMIN_PROFILE=/tmp/firefox-plugin/tmp-profile

if [ -d $CHEMIN_PROFILE ]
then
    echo "Utilisation du profile firefox situé sous le chemin: "$CHEMIN_PROFILE
    jpm run -b /usr/lib/firefox/firefox --no-copy --profile $CHEMIN_PROFILE
else
  #Récréer un profile neuf...
  #rm -Rf $CHEMIN_PROFILE/*

  mkdir -p $CHEMIN_PROFILE
  echo "Création du profile firefox sous le chemin: "$CHEMIN_PROFILE
  IDUTILISATEUR=`id --user --name`
  jpm run -b /usr/lib/firefox/firefox --no-copy --profile $CHEMIN_PROFILE &
  PID=$!
  sleep 10s
  KILL_PID=`ps -f --no-headers -o pid= --ppid $PID`
  echo
  echo " >>>>>>> Pour tuer le process: kill "$KILL_PID
  echo " >>>>>>> Le plugin en cours de développement n'étant pas signé, il vous faut modifier les paramètres de ce nouveau profile Firefox de manière à pouvoir l'activer."
  echo " >>>>>>> Rendez-vous à l'adresse 'about:config' depuis la barre d'adresse Firefox et appuyez sur le bouton indiquant votre compréhension des risques ;-)"
  echo " >>>>>>> Passez la veleur suivante à 'false': 'xpinstall.signatures.required'"
  echo " >>>>>>> Le plugin en développement sera alors actif !"
fi
